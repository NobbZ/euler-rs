use num::Num;

pub struct Fibs<I> {
    curr: I,
    next: I,
}

impl<I> Fibs<I>
where
    I: Num,
{
    pub fn new() -> Self {
        Fibs {
            curr: <I>::zero(),
            next: <I>::one(),
        }
    }
}

impl<I> Iterator for Fibs<I>
where
    I: Num + Copy,
{
    type Item = I;

    fn next(&mut self) -> Option<Self::Item> {
        let r = self.curr;

        self.curr = self.next;
        self.next = self.next + r;

        Some(r)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn fib_sequence() {
        let mut fibs = Fibs::new();

        assert_eq!(Some(0), fibs.next());
        assert_eq!(Some(1), fibs.next());
        assert_eq!(Some(1), fibs.next());
        assert_eq!(Some(2), fibs.next());
        assert_eq!(Some(3), fibs.next());
        assert_eq!(Some(5), fibs.next());
        assert_eq!(Some(8), fibs.next());
        assert_eq!(Some(13), fibs.next());
        assert_eq!(Some(21), fibs.next());
        assert_eq!(Some(34), fibs.next());
    }
}
