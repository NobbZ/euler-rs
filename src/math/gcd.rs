use num::Num;

#[inline]
pub fn gcd<N>(a: N, b: N) -> N
where
    N: Num + Copy,
{
    if b.is_zero() {
        return a;
    }

    gcd(b, a % b)
}
