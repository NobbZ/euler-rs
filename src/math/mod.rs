mod fib;
mod gcd;
mod prime;
mod scm;

pub use self::{fib::Fibs, gcd::gcd, prime::Primes, scm::scm};
