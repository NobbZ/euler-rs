use num::{Num, Signed};

use crate::math::gcd::gcd;

#[inline]
pub fn scm<N>(a: N, b: N) -> N
where
    N: Num + Signed + Copy,
{
    (a.abs() / gcd(a, b)) * b
}
