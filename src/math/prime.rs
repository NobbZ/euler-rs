use std::{ops::Generator, pin::Pin};

pub struct Primes {
    gen: Pin<Box<dyn Generator<Yield = u64, Return = ()> + Unpin>>,
}

impl Primes {
    pub fn new() -> Self {
        Primes {
            gen: Pin::new(Box::new(|| {
                let mut primes: Vec<u64> = vec![2, 3];
                yield 2;
                yield 3;
                for n in 1.. {
                    let m = 6 * n;
                    let (a, b) = (m - 1, m + 1);

                    if primes.iter().all(|&p| a % p != 0) {
                        primes.push(a);
                        yield a;
                    }

                    if primes.iter().all(|&p| b % p != 0) {
                        primes.push(b);
                        yield b;
                    }
                }
            })),
        }
    }
}

impl Iterator for Primes {
    type Item = u64;

    fn next(&mut self) -> Option<Self::Item> {
        use std::ops::GeneratorState::*;

        match self.gen.as_mut().resume() {
            Yielded(p) => Some(p),
            Complete(()) => unreachable!("You exhausted the primes"),
        }
    }
}

#[cfg(test)]
mod tests {
    use seq_macro::seq;

    use super::*;

    #[rustfmt::skip]
    const PRIMES: &[u64] = &[
        2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97
    ];

    seq!(N in 0..=24 {
        #[test]
        fn prime_#N() {
            assert_eq!(Primes::new().nth(N).unwrap(), PRIMES[N]);
        }
    });
}
