#![deny(clippy::all)]
#![cfg_attr(test, feature(test))]
#![feature(proc_macro_hygiene)]
#![feature(generators)]
#![feature(generator_trait)]

#[cfg(test)]
extern crate test;

mod math;

use std::{
    collections::hash_map::DefaultHasher,
    hash::{Hash, Hasher},
};

use colored::Colorize;
use seq_macro::seq;

fn hash<H>(val: H) -> u64
where
    H: Hash,
{
    let mut hasher = DefaultHasher::new();
    val.hash(&mut hasher);
    hasher.finish()
}

macro_rules! problems {
    ($($a:tt .. $b:tt,)*) => {

        $(seq!(N in $a..=$b {
            mod p#N;
        });)*

        pub fn run_all(spoil: bool) {
            if spoil {
                println!("{} {}", "WARNING!".bold().red(), "SPOILERS ACTIVE, PRINTING RAW RESULTS.".red());
            }
            $(
                seq!(N in $a..=$b {
                    p#N::run(spoil);
                });
            )*
        }
    };
}

macro_rules! problem {
    ($id:expr, $head:expr, $body:expr, $exp:expr,) => {
        #[cfg(test)]
        mod benches {
            use super::*;
            use crate::test::Bencher;

            problem!(@bench $exp);
        }

        #[cfg(test)]
        mod tests {
            use super::*;

            problem!(@test $exp);
        }

        pub fn run(spoil: bool) {
            use colored::Colorize;
            problem!(@print spoil $id, $head, $body, $exp);
        }
    };

    (@print $spoil:ident $id:expr, $head:expr, $body:expr, $exp:expr) => {
        let start = std::time::Instant::now();
        let result = solve();
        let duration = std::time::Instant::now().duration_since(start);

        println!(
            "{:>3}: solved in {:>9}. \"{}\"",
            format!("{}", $id).bold().yellow(),
            format!("{:2.3?}", duration).bold(),
            $head.green(),
        );

        if $spoil {
            println!( // TODO: Only calculate once and provide a timing!
                "{:>3}: <{:>20}> => {:10?}",
                format!("{}", $id).yellow(),
                format!("{}", $crate::hash(result)).bold().red(),
                result,
            )
        } else {
            println!(
                "{:>3}: <{:>20}>",
                format!("{}", $id).yellow(),
                format!("{}", $crate::hash(result)).bold().red(),
            )
        }
    };

    (@test $exp:expr) => {
        #[allow(clippy::unreadable_literal)]
        #[test]
        fn canonical_result() {
            assert_eq!($exp, $crate::hash(solve()));
        }
    };

    (@bench $exp:expr) => {
        #[bench]
        fn canonical_result(b: &mut Bencher) {
            b.iter(solve);
        }
    }


}

problems![1..7, 10..10,];
