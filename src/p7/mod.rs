use crate::math::Primes;

problem!{
    7,
    "10001st prime",
    r"By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?",
    12179447479031772572,
}

fn solve() -> u64 {
    Primes::new()
        .nth(10_000) // The text asks for the 10_001st, but .nth() is a zero based .at()
        .unwrap()
}
