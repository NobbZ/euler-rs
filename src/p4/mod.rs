use unicode_segmentation::UnicodeSegmentation;

problem! {
    4,
    "Largest palindrome product",
    r"A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.",
    9307404113722912529,
}

fn solve() -> u64 {
    (100..=999)
        .flat_map(|n| (n..=999).map(|m| (n, m)).collect::<Vec<_>>())
        .map(|(n, m)| n * m)
        .filter(|&n| is_palindrom(n))
        .max()
        .unwrap()
}

fn is_palindrom(s: u64) -> bool {
    let s1 = s.to_string();
    let s2 = s1
        .graphemes(true)
        .rev()
        .flat_map(str::chars)
        .collect::<String>();

    s1 == s2
}
