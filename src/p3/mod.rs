problem!{
    3,
    "Largest prime factor",
    r"The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143?",
    17527908647977189225,
}

fn solve() -> u64 {
    factors(600_851_475_143)
        .max()
        .expect("There were no prime factors")
}

fn factors(n: u64) -> impl Iterator<Item = u64> {
    PrimeFactors { m: 2, n: n }
}

struct PrimeFactors {
    n: u64,
    m: u64,
}

impl Iterator for PrimeFactors {
    type Item = u64;

    fn next(&mut self) -> Option<Self::Item> {
        if self.m > self.n {
            return None;
        };
        Some(self.next_factor())
    }
}

impl PrimeFactors {
    fn next_factor(&mut self) -> u64 {
        if self.n % self.m == 0 {
            self.n /= self.m;
            return self.m;
        }

        self.m += 1;

        self.next_factor()
    }
}
