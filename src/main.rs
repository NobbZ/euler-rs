#![deny(clippy::all)]

use std::env;

fn main() {
    let mut args = env::args();
    args.next(); // Skip the command

    let mut spoil = false; // If true exact solutions will be printed
    let mut filter = Vec::new();

    for arg in args {
        match arg.as_str() {
            "--spoil" => spoil = true,
            _ => filter.push(arg),
        }
    }

    let filter = filter.to_owned();

    if filter.is_empty() {
        euler_rs::run_all(spoil)
    }
}
