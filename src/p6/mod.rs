problem!{
    6,
    "Sum square difference",
    r"The sum of the squares of the first ten natural numbers is,

    1² + 2² + ... + 10² = 385
The square of the sum of the first ten natural numbers is,

    (1 + 2 + ... + 10)² = 552 = 3025
Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.",
    1876689871282851911,
}

fn solve() -> u64 {
    let sum: u64 = (1..=100).sum();
    let square_of_sum: u64 = sum * sum;
    let sum_of_square: u64 = (1..=100).map(|n| n * n).sum();

    square_of_sum - sum_of_square
}
