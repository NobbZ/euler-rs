use primal::Sieve;

problem!{
    10,
    "Summation of primes",
    r"Problem 10 
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.",
    13398904573042541180,
}

fn solve() -> u64 {
    // This is trivially solvable using crate::math::Primes, but it takes ~90
    // seconds on my mobile workstation, therefore I use primal::Sieve in the
    // final version.
    // PS: See also the benchmarks in this file.
    // Primes::new().take_while(|&p| p < 2_000_000).sum()
    const LIMIT: usize = 2_000_000;
    Sieve::new(LIMIT)
        .primes_from(1)
        .take_while(|&p| p < LIMIT)
        .map(|p| p as u64)
        .sum()
}

#[cfg(test)]
mod benchmarks {
    use primal::{Primes as PrimalPrimes, Sieve};

    use crate::{math::Primes, test::Bencher};

    const LIMIT: usize = 50_000;

    #[bench]
    fn use_my_primes(b: &mut Bencher) {
        b.iter(|| Primes::new().take_while(|&p| p < LIMIT as u64).sum::<u64>())
    }

    #[bench]
    fn use_primal_primes(b: &mut Bencher) {
        b.iter(|| {
            PrimalPrimes::all()
                .take_while(|&p| p < LIMIT)
                .map(|p| p as u64)
                .sum::<u64>()
        })
    }

    #[bench]
    fn use_primal_sieve(b: &mut Bencher) {
        b.iter(|| {
            Sieve::new(LIMIT)
                .primes_from(1)
                .take_while(|&p| p < LIMIT)
                .map(|p| p as u64)
                .sum::<u64>()
        })
    }
}
